<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Saloon Messenger</title>

        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
        <link rel="stylesheet" href="<?php echo $app->make('url')->to('/'); ?>/css/master.css">

    </head>
    <body>
        <div class="container">
            <div class="row" style="height: 65px;">
                <nav class="navbar navbar-default navbar-fixed-top">
                    <div class="container">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                <span class="sr-only">Saloon Messenger</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand" href="{{ route('url_to_salon') }}">Saloon Messenger</a>
                        </div>
                        <!-- Collect the nav links, forms, and other content for toggling -->
                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul class="nav navbar-nav  navbar-right">
                                <li>
                                    <a href="{{ route('url_to_salon') }}">
                                        Salons
                                    </a>
                                </li>
                                @if (session('role'))
                                    <li>
                                        <a href="{{ route('url_to_gestionSalons') }}">Gestionnaire</a>
                                    </li>
                                @endif
                                @if (session('pseudo'))
                                    <li>
                                        <a href="{{ route('url_to_profile') }}">Profil</a>
                                    </li>
                                @endif
                                <li>
                                    @if (session('pseudo'))
                                        <a href="{{ route('url_to_deconnexion') }}">Déconnexion</a>
                                    @else
                                        <a href="{{ route('url_to_connexion') }}">
                                            Connexion
                                        </a>
                                    @endif
                                </li>
                            </ul>
                        </div><!-- /.navbar-collapse -->
                    </div>
                </nav>
            </div>
