<!-- HEADER + MENU -->
@include ('includes/_header')

<h1  style="text-align: center;">
    Votre profil
</h1>

<div class="row margin_on">
    <div class="col-md-4 col-sm-6">
        <div class="col-sm-3 col-xs-12 user">
           Nom :
        </div>
       <div class="col-sm-9 col-xs-12 " >
           {{ $user -> lastname }}
       </div>
    </div>
    <div class="col-md-4 col-sm-6">
        <div class="col-sm-3 col-xs-12 user">
            Prénom :
        </div>
        <div class="col-sm-9 col-xs-12 " >
            {{ $user -> firstname }}
        </div>
    </div>
    <div class="col-md-4 col-sm-6">
        <div class="col-sm-3 col-xs-12  user">
            Pseudo :
        </div>
        <div class="col-sm-9 col-xs-12 " >
            {{ $user -> pseudo }}
        </div>
    </div>
    <div class="col-md-4 col-sm-6">
        <div class="col-sm-3 col-xs-12 user" >
           E-mail :
        </div>
        <div class="col-sm-9 col-xs-12 " >
            {{ $user -> email }}
        </div>
    </div>
    <div class="col-md-4 col-sm-6">
        <div class="col-sm-3 col-xs-12 user" >
           Rôle :
        </div>
        <div class="col-sm-9 col-xs-12 " >
            @if( $user->role == 0)
                User
            @else
                Admin
            @endif
        </div>
    </div>
</div>

<!-- FOOTER -->
@include ('includes/_footer')
