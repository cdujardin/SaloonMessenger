<!-- HEADER + MENU -->
@include ('includes/_header')

<h1  style="text-align: center;">
    Gestion des saloon
</h1>

<!-- Vous êtes bien connecté -->
@if (session('erreur_login'))
    <div class="success" style="color: green;">
        {{ session('erreur_login') }}
    </div>
@endif

@if (session('pseudo'))
    <div class="row hello margin_on">
        Bonjour {{session('pseudo')}} !
        @if(session('role') == 0)
              Vous êtes user !
        @else
            Vous êtes Admin !
        @endif
    </div>
@endif

<div class="row">
    <form class="" action="{{route('url_to_add_salon')}}" method="post">
        {{csrf_field()}}
        <input type="text" name="addSalon" value="" placeholder="Nom du salon à ajouter">
        <input class="btn btn-success" type="submit" name="add" value="Ajouter un salon">
    </form>
</div>

@if(count($salons) == 0)
    <!-- Si pas d'élément dans l'objet salon  -->
    <div class="row">
        <div class="col-xs-12 margin_on" style="color: #7e7e7e; font-size: 18px; text-align: center;">
            Il n'y a pas de salons disponible pour le moment... :(
        </div>
    </div>
@else


<!-- Sinon, on va faire une boucle pour les retrouver -->
    @foreach($salons as $salon)
        <div class="row salons_name">
            <div class="col-sm-3 col-xs-12">
                <a href="{{ route('url_to_selected_salon', $salon->id) }}">
                    {{ $salon -> name }}
                </a>
            </div>
            <div class="col-sm-9 col-xs-12">
                <!-- EN LARAVEL -->
                <!-- <form class="" action="{{route('url_to_modify_salon')}}" method="post">
                    {{csrf_field()}}
                    <input type="hidden" name="id" value="{{$salon->id}}">
                    <input type="text" name="name" value="">
                    <input class="btn btn-primary" type="submit" name="modifier" value="Modifier" >
                </form>
                <form class="" action="{{route('url_to_delete_salon')}}" method="post">
                    {{csrf_field()}}
                    <input type="hidden" name="id" value="{{$salon->id}}">
                    <input class="btn btn-danger" type="submit" name="delete" value="Supprimer">
                </form> -->

                <!-- EN AJAX -->
                <form class="" action="{{route('url_to_modify_salon')}}" method="post">
                    {{csrf_field()}}
                    <input type="hidden" name="id" value="{{$salon->id}}">
                    <input type="text" name="name" value="" placeholder="Nouveau nom !">
                    <input class="btn btn-primary" type="submit" name="modifier" value="Modifier" >
                </form>
                <form id="form_delete_salon" class="" action="javascript:void(0);" method="post">
                    {{csrf_field()}}
                    <input type="hidden" name="id" value="{{$salon->id}}">
                    <input type="hidden" name="name" value="{{$salon->name}}">
                    <input class="btn btn-danger" id="deleteSalon" type="submit" name="delete" value="Supprimer">
                </form>
            </div>
        </div>
    @endforeach
@endif

<!-- FOOTER -->
@include ('includes/_footer')


<script>
$( document ).ready(function() {
    // $('#deleteSalon').on('click', function(e){
    //     e.preventDefault();
    //
    // });

    $("#form_delete_salon").submit(function(){
        $.ajax({
            type:"POST",
            data: new FormData($(this)[0]),
            contentType:false,
            cache: false,
            processData:false,
            url:"./deleteSalonAjax",
            success: function(response){
                var objResponse = JSON.parse(response);
                if (objResponse.deleted) {
                    alert("Le salon "+objResponse.name+" est supprimé");
                    document.location.href="/gestionSalons";
                }else{
                    alert('Erreur');
                }

            },
            error: function(){
            }
        });
    });
});

</script>
