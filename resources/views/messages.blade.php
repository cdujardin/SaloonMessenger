<!-- HEADER + MENU -->
@include ('includes/_header')



<h1  style="text-align: center;">
    Liste des messages de {{$salons->name}}
</h1>

@if(session('pseudo'))
    <div class="row hello margin_on" >
        Bonjour {{session('pseudo')}} !
    </div>

    @if(count($messages) == 0)
    <!-- Si pas d'élément dans l'objet message  -->
    <div class="row">
        <div class="col-xs-12">
            Pas de messages disponible
        </div>
    </div>
    @elseif(session('role')==1)
        @foreach($messages as $message)
        <div class="row" style="border-bottom: 1px solid #e7e7e7;">
            <div class="col-md-2 col-xs-12 margin_on pseudo">
                {{ $message -> pseudo }} :
            </div>
            <div class="col-md-8 col-xs-12 margin_on">
                {{ $message -> message }}
            </div>
            <div class="col-md-2 col-xs-12">
                <form id="form_delete_message" class="" action="javascript:void(0);" method="post">
                    {{csrf_field()}}
                    <input type="hidden" name="id" value="{{$message->id}}">
                    <input type="hidden" name="message" value="{{$message->message}}">
                    <input class="btn btn-danger" id="deleteMessage" type="submit" name="delete" value="Supprimer">
                </form>
            </div>
        </div>
        @endforeach
    @else
    <!-- Sinon, on va faire une boucle pour les retrouver -->
        @foreach($messages as $message)
        <div class="row" style="border-bottom: 1px solid #e7e7e7;">
            <div class="col-xs-2 margin_on pseudo">
                {{ $message -> pseudo }} :
            </div>
            <div class="col-xs-10 margin_on">
                {{ $message -> message }}
            </div>
        </div>
        @endforeach
    @endif
    <div class="row">
        <form class="" action="{{route('url_to_send_message')}}" method="post">
            {{csrf_field()}}
            <input type="hidden" name="salon_id" value="{{$salons->id}}">
            <div class="col-md-10 col-xs-12 margin_on">
                <input type="text" name="message" value="" placeholder="Message" style="width: 100%; height: 40px; padding: 10px;">
            </div>
            <div class="col-md-2 col-xs-12 margin_on">
                <input class="btn btn-success" type="submit" name="submit" value="Envoyer !">
            </div>
        </form>
    </div>
    <!-- Vous êtes bien connecté -->
    @if (session('erreur_login'))
        <div class="error" style="color: red;">
            {{ session('erreur_login') }}
        </div>
    @endif
@else
    <div class="row" style="color: red; margin-top: 20px; margin-bottom: 20px;">
        Vous devez être connecté !
    </div>
@endif


<!-- FOOTER -->
@include ('includes/_footer')

<script>
$( document ).ready(function() {
    $("#form_delete_message").submit(function(){
        $.ajax({
            type:"POST",
            data: new FormData($(this)[0]),
            contentType:false,
            cache: false,
            processData:false,
            url:"/deleteMessageAjax",
            success: function(response){
                var objResponse = JSON.parse(response);
                if (objResponse.deleted) {
                    alert("Le message "+objResponse.message+" est supprimé");
                    document.location.href="/salon/{{$salons->id}}";
                }else{
                    alert('Erreur');
                }

            },
            error: function(){
            }
        });
    });
});

</script>
