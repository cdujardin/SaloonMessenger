<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class ProfileController extends Controller
{
    public function profile(Request $rq){
        $user = User::where('id', "=", $rq->session()->get('id_user'))->first();

        // var_dump($salon);
        return view('profile')->with('user', $user);
    }
}
