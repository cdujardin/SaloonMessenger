<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Message;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;

class MessageController extends Controller
{
    public function sendMessage(Request $rq){
        if($rq->has('message')==false){
            $erreur_login = 'Vous n\'avez rien écrit !';
            return back()->with('erreur_login', $erreur_login);
            // retour vers la même page mais on affiche un message d'erreur
        }else {
            $message = new Message();
            $message->salon_id = $rq->input('salon_id');
            $message->message = $rq->input('message');
            $message->user_id = $rq->session()->get('id_user');
            $message->save();

            $erreur_login = 'Message envoyé !';
            return back()->with('erreur_login', $erreur_login);
            // retour vers la même page mais on affiche un message d'erreur
        }
    }

    public function deleteMessageAjax(Request $rq){
        $dataResponse = array();

        $id = Input::get('id');
        $message = Input::get('message');
        $deleteMessage = Message::where('id', "=", $id)->delete();

        $dataResponse['deleted'] = true;
        $dataResponse['id'] = $id;
        $dataResponse['message'] = $message;

        return json_encode($dataResponse);
    }
}
